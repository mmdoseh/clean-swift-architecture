# Change Log

All notable changes to this project will be documented in this file

## 1.0.1

- Removed Scene unsupported components:
- Updated documentaion of code. 
    

## 1.0.0

- Added the Scene template to generate the following Clean Swift components:
	- View Controller
	- Interactor
	- Presenter
	- Router
	- Models
	- Configurator
     - IRouter
     - Protocol
    
- These components can also be generated individually
