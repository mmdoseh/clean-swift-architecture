//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//  Clean Swift Architecture, Template Autor Dose.

import UIKit


//MARK: - ___VARIABLE_productName:identifier___Model

/// Structure ___VARIABLE_productName:identifier___Model that contains models for `Interactor`, `Pressenter` and `Displayable Controller`
///
/// Models can be changed by you, if you want to add other models, write an extensions.
struct ___VARIABLE_productName:identifier___Model {	
    
    /// Model for `Interactor` that should contain parameters for fetching data
    struct Request {

    }
    /// Model for `Pressenter` that should contain parameters for Pressenting Interacted data.
    struct Response {

    }
    /// Model for `Controller Displayable` that should contain viewModel for Displaying Pressented data.
    struct ViewModel {

    }
}


//MARK: - Other Supported Models

extension ___VARIABLE_productName:identifier___Model {
    
}
