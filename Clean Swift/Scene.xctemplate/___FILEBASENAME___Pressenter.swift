//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//  Clean Swift Architecture, Template Autor Dose.

import UIKit


//MARK: - ___VARIABLE_productName:identifier___ Pressenter

///  is an presenter responsible for presenting logic.
final class ___VARIABLE_productName:identifier___Pressenter {
    
	private(set) weak var controller: ___VARIABLE_productName:identifier___Displayable?
	
    
    // MARK: - Initializers
    
    /// Initializes a new instance of ___VARIABLE_productName:identifier___Pressenter with an output object
    ///
    /// - parameter controller: The controller that conforms to protocol ___VARIABLE_productName:identifier___Displayable
    ///
    /// - returns: The instance of ___VARIABLE_productName:identifier___Pressenter
	init(controller: ___VARIABLE_productName:identifier___Displayable?) {
		self.controller = controller
	}
}


//MARK: - ___VARIABLE_productName:identifier___Pressentable

extension ___VARIABLE_productName:identifier___Pressenter: ___VARIABLE_productName:identifier___Pressentable {
    
    func pressent(response: ___VARIABLE_productName:identifier___Model.Response) {
        
    }


}

