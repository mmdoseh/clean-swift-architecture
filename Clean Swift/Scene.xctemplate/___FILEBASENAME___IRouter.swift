//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//  Clean Swift Architecture, Template Autor Dose.

import UIKit


//MARK: - ___VARIABLE_productName:identifier___IRoute

/// Enum of iRoutable items, that contains controllers and pramateres for base setup of Controller
enum ___VARIABLE_productName:identifier___IRoute: IRouter {
    

    var module: UIViewController? {
        return UIViewController()
    }
}



