//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//  Clean Swift Architecture, Template Autor Dose.

import UIKit


//MARK: - ___VARIABLE_productName:identifier___ Interactor

/// ___VARIABLE_productName:identifier___ `Interacor` is an interactor responsible for fetching  data.
final class ___VARIABLE_productName:identifier___Interactor {
    
   private(set) var pressenter: ___VARIABLE_productName:identifier___Pressentable?
    
    
    // MARK: - Initializers
    
    /// Initializes a new instance of ___VARIABLE_productName:identifier___ `Interactor`
    ///
    /// - parameter pressenter: The pressenter that conforms to protocol ___VARIABLE_productName:identifier___Pressentable
    ///
    /// - returns: The instance of ___VARIABLE_productName:identifier___Interactor
    init(pressenter: ___VARIABLE_productName:identifier___Pressentable) {
    	self.pressenter = pressenter
    }
}


// MARK: - ___VARIABLE_productName:identifier___Interactable

extension ___VARIABLE_productName:identifier___Interactor: ___VARIABLE_productName:identifier___Interactable {
   
    func fetch(with request: ___VARIABLE_productName:identifier___Model.Request) {

    }

}
