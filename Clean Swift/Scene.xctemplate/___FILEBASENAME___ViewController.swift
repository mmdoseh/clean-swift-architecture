//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//  Clean Swift Architecture, Template Autor Dose.

import UIKit


//MARK: - ___VARIABLE_productName:identifier___ViewController

final class ___VARIABLE_productName:identifier___ViewController: UIViewController, StoryboardInitializable {
    
    //MARK: - @IBOultets

    
    //MARK: - Router, Interactor which should be configed in Configurator
    
    var interactor: ___VARIABLE_productName:identifier___Interactor?
    var router: I___VARIABLE_productName:identifier___Router?
    
    
    //MARK: - Properties


    // MARK: - Initializers
       
       /// Initializes an instance of ItemCategoriesViewController from storyboard
       ///
       /// - parameter coder: The coder
       ///
       /// - returns: The instance of ItemCategoriesViewController
     required init?(coder: NSCoder) {
         super.init(coder: coder)
         config()
     }
     
     ///`Using just for test Configurator `
     init() {
         super.init(nibName: nil, bundle: nil)
         config()
     }
     

    // MARK: - View lifecyrcle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    //MARK: - Configurator
    
    private func config() {
        ___VARIABLE_productName:identifier___Configurator.configController(controller: self)
    }


    //MARK: - Functions

}


//MARK: - ___VARIABLE_productName:identifier___Displayable

extension ___VARIABLE_productName:identifier___ViewController: ___VARIABLE_productName:identifier___Displayable {
    
    func display(viewModel model: ___VARIABLE_productName:identifier___Model.ViewModel) {
        
    }
}
