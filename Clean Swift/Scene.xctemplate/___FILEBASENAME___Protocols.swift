//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//  Clean Swift Architecture, Template Autor Dose.

import UIKit


//MARK: - ___VARIABLE_productName:identifier___ Protocols

/// ___VARIABLE_productName:identifier___Displayable is responsable to display viewModel.
protocol ___VARIABLE_productName:identifier___Displayable: class, ErrorDisplayable {
    /// Display  `pressented` viewModel
    /// - Parameter viewModel: `Pressented ViewModel`
    func display(viewModel: ___VARIABLE_productName:identifier___Model.ViewModel)
}

/// ___VARIABLE_productName:identifier___Pressentable is responsable to pressent logic to display.
protocol ___VARIABLE_productName:identifier___Pressentable: class {
    /// Pressent Interactable Data
    /// - Parameter model: `Response or Entity Model for pressenter`
    func pressent(response: ___VARIABLE_productName:identifier___Model.Response)
}

/// ___VARIABLE_productName:identifier___Interactable is responsable to interact buissness logic.
protocol ___VARIABLE_productName:identifier___Interactable: class {
    /// Fetch specified data from `Services` or `Workers`, eg. Network callBacks.
    /// - Parameter request: `Request Model containig information to fetch data.`
    func fetch(with request: ___VARIABLE_productName:identifier___Model.Request)
}

