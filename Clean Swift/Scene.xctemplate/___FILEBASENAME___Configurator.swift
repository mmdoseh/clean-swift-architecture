//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//  Clean Swift Architecture, Template Autor Dose.

import UIKit


//MARK: - ___VARIABLE_productName:identifier___Configurator

struct ___VARIABLE_productName:identifier___Configurator {
    
    /// The Configurator of ViewController, that setup it from storyboard and return it.
    /// - Parameter model: `Optional Model for base Setup`
    /// - Returns: `Controller` that setupped from storyboard and ready to `Show`
    static func config(with model: Any? = nil) -> UIViewController {
        let controller = ___VARIABLE_productName:identifier___ViewController()
        let router = ___VARIABLE_productName:identifier___Router(controller: controller)
        let pressenter: ___VARIABLE_productName:identifier___Pressentable = ___VARIABLE_productName:identifier___Pressenter(controller: controller)
        let interactor = ___VARIABLE_productName:identifier___Interactor(pressenter: pressenter)
        controller.interactor = interactor
        controller.router = router
        return controller
    }
    
    /// The **Base** configs for Controller. Required to contain `Interactor` and `Router`
    /// - Parameter controller: `Controller` that should be setuped
    static func configController(controller: ___VARIABLE_productName:identifier___ViewController) {
        
        controller.router = ___VARIABLE_productName:identifier___Router(controller: controller)
        
        let pressenter = ___VARIABLE_productName:identifier___Pressenter(controller: controller)
        let interactor = ___VARIABLE_productName:identifier___Interactor(pressenter: pressenter)
        
        controller.interactor = interactor
    }
    
}
