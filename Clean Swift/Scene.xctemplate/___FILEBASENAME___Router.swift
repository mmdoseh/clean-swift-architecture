//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.
//  Clean Swift Architecture, Template Autor Dose.

import UIKit


//MARK: - ___VARIABLE_productName:identifier___Router Protocols

/// I___VARIABLE_productName:identifier___Router is an routers protocol .
protocol I___VARIABLE_productName:identifier___Router: class {
    
    var controller: UIViewController? { get }
    
    /// Route or navigate controller
    /// - Parameter module: Module of controller, it may contain data and other parameters for base setup of controller.
    func route(to module: ___VARIABLE_productName:identifier___IRoute)
}


//MARK: - ___VARIABLE_productName:identifier___Router

/// ___VARIABLE_productName:identifier___Router is an router that is responsable to navigate and route controller .
final class ___VARIABLE_productName:identifier___Router {
    
	private(set) weak var controller: UIViewController?
    
    // MARK: - Initializers
       
       /// Initializes a new instance of ___VARIABLE_productName:identifier___Router
       ///
       /// - parameter controller: The controller that should be route.
       ///
       /// - returns: The instance of ___VARIABLE_productName:identifier___Router
	init(controller: UIViewController?) {
		self.controller = controller
	}
}


//MARK: - I___VARIABLE_productName:identifier___Router

extension ___VARIABLE_productName:identifier___Router: I___VARIABLE_productName:identifier___Router {
    
    func route(to module: ___VARIABLE_productName:identifier___IRoute) {
        
    }
}
