
Swift 5.2
XCode 11+

# Clean Swift Architecture

Clean Swift (a.k.a VIP) is Uncle Bob's Clean Architecture applied to iOS and Mac projects. The Clean Swift Architecture is not a framework. It is a set of Xcode templates to generate the **Clean Architecture** components for you. 

> I Add some changes in Architecture and made it more Important ! 

Changes: 

``` 
Configurator: Replace instances construction place, moved in Configurator. 

Protocols: Controller - Interactor - Pressenter conforms Protocols which made code more Clean 

Router: Route functionality is Responsable with IRouter model, which contains `modul(it configs controllers)
`
More: ...  

```

## Installation 
  
  - Download the Template
  - Install the Template

**After unzipping the file, you should open Terminal and access the root directory of the folder by using cd CleanSwift .
Now, all you have to do is running the following command `make install`. If you wish to uninstall the template just run `make uninstall`.
To verify if the template has been installed properly, you should open the following path` ~Library/Developer/Xcode/Templates/File` Templates and search for a folder named CleanSwift.**
```
